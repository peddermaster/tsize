#![warn(rust_2018_idioms)]

#[cfg(windows)]
use std::io::Error;

#[cfg(not(windows))]
use termion::terminal_size;

#[cfg(windows)]
fn terminal_size() -> Result<(u16, u16), Error> {
    use winapi::um::{
        processenv::GetStdHandle,
        winbase::STD_OUTPUT_HANDLE,
        wincon::{GetConsoleScreenBufferInfo, CONSOLE_SCREEN_BUFFER_INFO, COORD, SMALL_RECT},
        winnt::HANDLE,
    };

    let hand: HANDLE = unsafe { GetStdHandle(STD_OUTPUT_HANDLE) };

    let zc = COORD { X: 0, Y: 0 };
    let mut csbi = CONSOLE_SCREEN_BUFFER_INFO {
        dwSize: zc,
        dwCursorPosition: zc,
        wAttributes: 0,
        srWindow: SMALL_RECT {
            Left: 0,
            Top: 0,
            Right: 0,
            Bottom: 0,
        },
        dwMaximumWindowSize: zc,
    };
    let success: bool = unsafe { GetConsoleScreenBufferInfo(hand, &mut csbi) != 0 };
    if success {
        let w = (csbi.srWindow.Right - csbi.srWindow.Left + 1) as u16;
        let h = (csbi.srWindow.Bottom - csbi.srWindow.Top + 1) as u16;
        Ok((w, h))
    } else {
        Err(Error::last_os_error())
    }
}

fn main() {
    match terminal_size() {
        Ok((w, h)) => println!("{} {}", w, h),
        Err(err) => {
            eprintln!("{}", err);
            println!("0 0");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_does_not_crash() {
        let r = terminal_size();
        eprintln!("{:?}", r);
    }
}
